import React from 'react';
import { Text } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import I18n from './config/i18n';
import { MAIN_COLOR, SECONDARY_COLOR } from './AppConstants';

import SplashScreen from './screens/splash/SplashScreen';
import LoginScreen from './screens/login/LoginScreen';
import UserListScreen from './screens/user/UserListScreen';
import UserProfileScreen from './screens/user/UserProfileScreen';
import EventListScreen from './screens/events/EventListScreen';
import EventDetailsScreen from './screens/events/EventDetailsScreen';

const TabIcon = ({ selected, title }) => (
    <Text style={{ color: selected ? 'white' : 'white' }}>{title}</Text>
);

const Scenes = {
    SPLASH_SCENE: 'splashScene',
    AUTH_SCENE: 'authScene',
    MAIN_SCENE: 'mainScene',
    EVENT_TAB_SCENE: 'eventTabScene',
};


/**
 * Example usage in other components
 * 
 * Actions.userProfileScreen();
 * 
 * Notice that the scene key matches the action method name!
 */

const Screens = {
    SPLASH_SCREEN: 'splashScreen',
    LOGIN_SCREEN: 'loginScreen',
    REGISTRATION_SCREEN: 'registrationScreen',
    USER_PROFILE_SCREEN: 'userProfileScreen',
    USER_LIST_SCREEN: 'userListScreen',
    EVENT_LIST_SCREEN: 'eventListScreen',
    EVENT_DETAILS_SCREEN: 'eventDetailsScreen',
};

/**
 * Navigation bar actions
 */
const actionProfile = () => Actions.userProfileScreen({ fetchCurrentUser: true });

const RouterComponent = () => (
    <Router
        sceneStyle={{ backgroundColor: MAIN_COLOR }}
    >

        <Scene
            initial
            key={Scenes.SPLASH_SCENE}
            navigationBarStyle={styles.authNavigationBarStyle}
        >
            <Scene key={Screens.SPLASH_SCREEN} component={SplashScreen} />
        </Scene>

        <Scene
            key={Scenes.AUTH_SCENE}
            navigationBarStyle={styles.authNavigationBarStyle}
        >
            <Scene key={Screens.LOGIN_SCREEN} component={LoginScreen} />
            {createScene(false, Screens.REGISTRATION_SCREEN, UserProfileScreen, I18n.t('screen_name_register'))}
        </Scene>

        <Scene
            key={Scenes.MAIN_SCENE}
            navigationBarStyle={styles.mainNavigationBarStyle}
            titleStyle={styles.navBarTitle}
            barButtonTextStyle={styles.barButtonTextStyle}
            barButtonIconStyle={styles.barButtonIconStyle}
            leftButtonIconStyle={styles.leftButtonIconStyle}
            onRightAction={actionProfile}
        >
            {createScene(true, Screens.EVENT_LIST_SCREEN, EventListScreen, I18n.t('screen_name_events'), 'Profile')}
            {createScene(false, Screens.USER_PROFILE_SCREEN, UserProfileScreen, I18n.t('screen_name_profile'))}

            <Scene
                key={Scenes.EVENT_TAB_SCENE}
                tabs
                navigationBarStyle={styles.mainNavigationBarStyle}
                tabBarStyle={styles.mainNavigationBarWithTabsStyle}
            >
                {createTabScene(true, Screens.EVENT_DETAILS_SCREEN, EventDetailsScreen, I18n.t('screen_name_event_details'))}
                {createTabScene(false, Screens.USER_LIST_SCREEN, UserListScreen, I18n.t('screen_name_users'))}
            </Scene>

        </Scene>

    </Router >
);

function createScene(initial, key, component, title, rightNavigationBarTitle, onRightAction) {
    return (<Scene
        initial={initial}
        key={key}
        component={component}
        title={title}
        onRight={() => onRightAction}
        rightTitle={rightNavigationBarTitle}
        navigationBarStyle={styles.mainNavigationBarStyle}
        titleStyle={styles.navBarTitle}
        barButtonTextStyle={styles.barButtonTextStyle}
        barButtonIconStyle={styles.barButtonIconStyle}
        leftButtonTextStyle={styles.barButtonTextStyle}
        leftButtonIconStyle={styles.leftButtonIconStyle}
        rightButtonTextStyle={styles.barButtonTextStyle}
        rightButtonIconStyle={styles.barButtonIconStyle}
    />);
}

function createTabScene(initial, key, component, title) {
    return (<Scene
        initial={initial}
        key={key}
        component={component}
        title={title}
        icon={TabIcon}
        navigationBarStyle={styles.mainNavigationBarStyle}
        titleStyle={styles.navBarTitle}
        barButtonTextStyle={styles.barButtonTextStyle}
        barButtonIconStyle={styles.barButtonIconStyle}
        leftButtonTextStyle={styles.barButtonTextStyle}
        leftButtonIconStyle={styles.leftButtonIconStyle}
        rightButtonTextStyle={styles.barButtonTextStyle}
        rightButtonIconStyle={styles.barButtonIconStyle}
    />);
}

const styles = {
    authNavigationBarStyle: {
        backgroundColor: 'transparent',
        borderBottomWidth: 0,
    },
    mainNavigationBarStyle: {
        backgroundColor: SECONDARY_COLOR,
        borderBottomWidth: 0
    },
    mainNavigationBarWithTabsStyle: {
        backgroundColor: SECONDARY_COLOR,
        borderBottomWidth: 0,
    },
    tabBarStyle: {
        borderTopWidth: 0.5,
        backgroundColor: SECONDARY_COLOR,
        opacity: 1
    },
    navBarTitle: {
        color: '#FFFFFF'
    },
    barButtonTextStyle: {
        color: '#FFFFFF'
    },
    barButtonIconStyle: {
        tintColor: '#FFFFFF'
    },
    leftButtonIconStyle: {
        tintColor: '#FFFFFF'
    }
};

export default RouterComponent;
