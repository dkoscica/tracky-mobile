import React, { Component } from 'react';
import { TextInput, View, Text } from 'react-native';
import { isStringLengthValid, isStringRegexValid } from '../../utils/StringUtils';

class EditTextField extends Component {

    static propTypes = {
        label: React.PropTypes.string,
        minTextLength: React.PropTypes.number,
        maxTextLength: React.PropTypes.number,
        validationRegex: React.PropTypes.object,
        errorMessage: React.PropTypes.string,
        value: React.PropTypes.string,
        placeholder: React.PropTypes.string,
        onChangeText: React.PropTypes.func,
        secureTextEntry: React.PropTypes.bool,
        error: React.PropTypes.bool,
        editable: React.PropTypes.bool
    }

    constructor() {
        super();
        this.state = {
            showError: false
        };
    }

    onTextChange(text) {
        const { minTextLength, maxTextLength, validationRegex } = this.props;
        if (text && isStringLengthValid(text, minTextLength, maxTextLength) && isStringRegexValid(text, validationRegex)) {
            this.setState({
                showError: false
            });
        } else {
            this.setState({
                showError: true
            });
        }
    }

    render() {
        const { editable, label, errorMessage, value, placeholder, onChangeText, secureTextEntry } = this.props;
        const { inputStyle, labelStyle, errorStyle, containerVerticalStyle, containerHorizontalStyle } = styles;

        return (
            <View style={containerVerticalStyle}>
                <View style={containerHorizontalStyle}>
                    <Text style={labelStyle}>{label}</Text>
                    <TextInput
                        editable={editable}
                        autoCapitalize='none'
                        secureTextEntry={secureTextEntry}
                        placeholder={placeholder}
                        autoCorrect={false}
                        style={inputStyle}
                        underlineColorAndroid='transparent'
                        value={value}
                        onChangeText={onChangeText}
                    />
                </View>
                {this.state.showError && <Text style={errorStyle}>{errorMessage}</Text>}
            </View>
        );
    }
}

const styles = {

    inputStyle: {
        color: '#000',
        fontSize: 14,
        lineHeight: 20,
        flex: 2 //Similar to weight in Android LinearLayout
    },
    labelStyle: {
        fontSize: 14,
        fontWeight: '600',
        paddingLeft: 10,
        paddingRight: 15,
        flex: 1
    },
    errorStyle: {
        height: 20,
        fontSize: 14,
        color: '#C44741',
        paddingRight: 10
    },
    containerVerticalStyle: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end',
        //borderColor: '#FFF223',
        //borderWidth: 1,
        //borderStyle: 'solid',
    },
    containerHorizontalStyle: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export { EditTextField };
