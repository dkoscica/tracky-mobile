import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

const TextField = ({ children, textStyle }) => {
  return (
    <Text style={[styles.textStyle, textStyle]}>
      {children}
    </Text>
  );
}

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 12,
    paddingBottom: 12
  }
};

export { TextField };