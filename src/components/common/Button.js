import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children, buttonStyle, textStyle }) => {
    return (
        <TouchableOpacity onPress={onPress} style={[styles.buttonStyle, buttonStyle]}>
            <Text style={[styles.textStyle, textStyle]} >{children}</Text>
        </TouchableOpacity>
    );
};

const styles = {

    textStyle: {
        textAlign: 'center',
        alignSelf: 'center',
        color: '#fff',
        fontSize: 16,
        fontWeight: '500',
    },

    buttonStyle: {
        height: 40,
        width: 150,
        paddingTop: 10,
        alignSelf: 'center',
        backgroundColor: '#007aff',
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,
    }
};

export { Button };
