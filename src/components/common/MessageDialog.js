import React from 'react';
import { Text, View, Modal } from 'react-native';
import { Button } from './Button';

const MessageDialog = ({ children, visible, onAccept }) => {

    const { modalContainerStyle, containerStyle, textStyle, buttonStyle } = styles;

    return (
        <Modal
            visible={visible}
            transparent
            animationType="fade"
            onRequestClose={() => { }}
        >
            <View style={containerStyle}>
                <View style={modalContainerStyle}>
                    <Text style={textStyle}>{children}</Text>
                    <Button style={buttonStyle} onPress={onAccept}>Ok</Button>
                </View>
            </View>
        </Modal>
    );
};

const styles = {
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center'
    },
    modalContainerStyle: {
        backgroundColor: '#fff',
        margin: 20,
        padding: 5,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#fff'
    },
    textStyle: {
        flexDirection: 'row',
        fontSize: 16,
        margin: 10,
        color: 'black',
        textAlign: 'center',
    },
    buttonStyle: {
        margin: 10
    },
};

export { MessageDialog };
