import React from 'react';
import { Image } from 'react-native';

const RoundedImage = ({ imageSource, imageSize }) => {
    return (
        <Image
            source={imageSource}
            style={{ width: imageSize, height: imageSize, alignSelf: 'center', borderRadius: 50 }}
        />
    );
};

export { RoundedImage };
