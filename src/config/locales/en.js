const en = {

  /**
   * Login
   */
  introduction_title: 'Tracky',
  email: 'Email',
  email_error: 'Email is required!',
  email_placeholder: 'email@gmail.com',
  password: 'Password',
  password_placeholder: 'password',
  login: 'Login',
  register: 'Register',

  /**
   * Profile
   */
  first_name: 'First name',
  first_name_placeholder: 'Jane',
  first_name_error: 'Please enter your First name!',
  last_name: 'Last name',
  last_name_placeholder: 'Foster',
  last_name_error: 'Please enter your Last name!',
  phone_number: 'Phone',
  phone_number_placeholder: '555-555-555',
  phone_number_error: 'Please enter your phone number!',
  save_changes: 'Save Changes',


  /**
   * Event
   */
  event_name: 'Event name',
  participants: 'Participants',
  secret_code: 'Secret code',
  location_name: 'Location name',
  address: 'Address',
  latitude: 'Latitude',
  longitude: 'Longitude',
  startDateTime: 'Start date time:',
  endDateTime: 'End date time:',
  registered_participants_count: 'Registered participants count',
  join_event: 'Join event',
  join_event_successful: 'You have successfully joined the event',
  join_event_failed: "Unfortunately you can't join the event since your determined location is not close enough to the event!",

  /**
   * Screen names
   */
  screen_name_register: 'Register',
  screen_name_profile: 'Profile',
  screen_name_events: 'Events',
  screen_name_event_details: 'Event Details',
  screen_name_users: 'Users',
  screen_name_user_details: 'User Details',

};

export default en;
