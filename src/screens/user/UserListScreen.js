import React, { Component } from 'react';
import { ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { usersFetch } from '../../actions';
import UserListItem from './UserListItem';
import { PLATFORM_SPECIFIC_CONTENT_INSET, PLATFORM_SPECIFIC_CONTENT_OFFSET } from '../../AppConstants';

class UserListScreen extends Component {

    componentWillMount() {
        this.props.usersFetch();
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(nextProps) {
        // nextProps are the next set of props that this component
        // will be rendered with
        // this.props is still the old set of props
        this.createDataSource(nextProps);
    }

    createDataSource({ users }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(users);
    }

    renderRow(user) {
        return <UserListItem user={user} />;
    }

    render() {
        return (
            <ListView
                contentInset={PLATFORM_SPECIFIC_CONTENT_INSET}
                contentOffset={PLATFORM_SPECIFIC_CONTENT_OFFSET}
                style={{ margin: 10, backgroundColor: 'transparent' }}
                enableEmptySections
                dataSource={this.dataSource}
                renderRow={this.renderRow}
            />
        );
    }
}

const mapStateToProps = state => {
    const users = _.map(state.users, (val, uid) => ({ ...val, uid }));
    return { users };
};

export default connect(mapStateToProps, { usersFetch })(UserListScreen);
