import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import I18n from '../../config/i18n';
import _ from 'lodash';
import KeychainManager from '../../managers/KeychainManager';
import { createUser, userUpdate, fetchLoggedInUser, userClearForm } from '../../actions';
import { Card, Button } from '../../components/common';
import UserForm from './UserForm';
import { PLATFORM_SPECIFIC_CONTENT_INSET, PLATFORM_SPECIFIC_CONTENT_OFFSET } from '../../AppConstants';

class UserProfileScreen extends Component {

    constructor(props) {
        super(props);
        this.state = { buttonTitle: '' };
        KeychainManager.getCredentials().then(credentials => {
            const buttonTitle = credentials ? I18n.t('save_changes') : I18n.t('register');
            this.setState({ buttonTitle });
            if (credentials) {
                this.props.fetchLoggedInUser();
            }
        });
    }

    componentDidMount() {
        _.each(this.props.user, (value, prop) => {
            this.props.userUpdate({ prop, value });
        });
    }

    onButtonPress() {
        const { firstName, lastName, email, password, phoneNumber } = this.props;
        this.props.createUser(firstName, lastName, email, password, phoneNumber);
    }

    renderCreateUpdateButton() {
        return (
            <Button
                buttonStyle={{ marginTop: 20, marginBottom: 20 }}
                onPress={this.onButtonPress.bind(this)}
            >
                {this.state.buttonTitle}
            </Button>
        );
    }

    render() {
        return (
            <ScrollView
                contentInset={PLATFORM_SPECIFIC_CONTENT_INSET}
                contentOffset={PLATFORM_SPECIFIC_CONTENT_OFFSET}
                style={{ margin: 10 }}
            >
                <Card>
                    <UserForm {...this.props} />
                </Card>
                {this.props.isUserProfileEditable ? this.renderCreateUpdateButton() : null}
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    const { firstName, lastName, email, password, phoneNumber } = state.userDetails;
    return { firstName, lastName, email, password, phoneNumber };
};

export default connect(mapStateToProps, { createUser, userUpdate, fetchLoggedInUser, userClearForm })(UserProfileScreen);
