import React, { Component } from 'react';
import { View } from 'react-native';
import I18n from '../../config/i18n';
import { connect } from 'react-redux';
import { userUpdate } from '../../actions';
import { CardSection, EditTextField } from '../../components/common';

class UserForm extends Component {

    renderPasswordField() {
        if (this.props.isUserProfileEditable) {
            return (<CardSection>
                <EditTextField
                    secureTextEntry
                    label={I18n.t('password')}
                    placeholder={I18n.t('password_placeholder')}
                    value={this.props.password}
                    onChangeText={value => this.props.userUpdate({ prop: 'password', value })}
                />
            </CardSection>);
        }
    }

    render() {
        return (
            <View>
                <CardSection>
                    <EditTextField
                        label={I18n.t('first_name')}
                        placeholder={I18n.t('first_name_placeholder')}
                        errorMessage={I18n.t('first_name_error')}
                        value={this.props.firstName}
                        onChangeText={value => this.props.userUpdate({ prop: 'firstName', value })}
                    />
                </CardSection>

                <CardSection>
                    <EditTextField
                        label={I18n.t('last_name')}
                        placeholder={I18n.t('last_name_placeholder')}
                        errorMessage={I18n.t('last_name_error')}
                        value={this.props.lastName}
                        onChangeText={value => this.props.userUpdate({ prop: 'lastName', value })}
                    />
                </CardSection>

                <CardSection>
                    <EditTextField
                        label={I18n.t('email')}
                        placeholder={I18n.t('email_placeholder')}
                        errorMessage={I18n.t('email_error')}
                        value={this.props.email}
                        onChangeText={value => this.props.userUpdate({ prop: 'email', value })}
                    />
                </CardSection>

                {this.renderPasswordField()}

                <CardSection>
                    <EditTextField
                        label={I18n.t('phone_number')}
                        placeholder={I18n.t('phone_number_placeholder')}
                        errorMessage={I18n.t('phone_number_error')}
                        value={this.props.phoneNumber}
                        onChangeText={value => this.props.userUpdate({ prop: 'phoneNumber', value })}
                    />
                </CardSection>
            </View>
        );
    }
}

const styles = {

};

const mapStateToProps = (state) => {
    const { firstName, lastName, email, password, phoneNumber } = state.userForm;
    return { firstName, lastName, email, password, phoneNumber };
};

export default connect(mapStateToProps, { userUpdate })(UserForm);
