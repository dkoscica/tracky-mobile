import React, { Component } from 'react';
import { View, Text } from 'react-native';
import I18n from '../../config/i18n';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../../actions';
import { Card, CardSection, EditTextField, Button, Spinner, TextField, RoundedImage } from '../../components/common';
import { SECONDARY_COLOR, GREEN_COLOR, EMAIL_REGEX } from '../../AppConstants';

class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            emailError: '',
            password: '',
            passwordError: ''
        };
    }

    /**
     * Event handlers
     */
    onEmailChange(text) {
        this.refs.emailEditTextField.onTextChange(text);
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onLoginPressed() {
        const { email, password } = this.props;
        this.props.loginUser({ email, password });
    }

    onRegisterPressed() {
        Actions.registrationScreen({ isUserProfileEditable: true });
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }
        return (
            <View>
                <Button
                    buttonStyle={{ backgroundColor: GREEN_COLOR, marginRight: 30, marginLeft: 30, marginBottom: 15 }}
                    onPress={this.onLoginPressed.bind(this)}
                >
                    {I18n.t('login')}
                </Button>
                <Button
                    buttonStyle={{ backgroundColor: SECONDARY_COLOR, marginRight: 30, marginLeft: 30 }}
                    onPress={this.onRegisterPressed.bind(this)}
                >
                    {I18n.t('register')}
                </Button>
            </View>
        );
    }

    render() {
        return (
            <View style={{ marginTop: 50 }}>
                <RoundedImage
                    imageSource={{ uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/9/92/React-native-icon.svg/1280px-React-native-icon.svg.png' }}
                    imageSize={100}
                />
                <TextField textStyle={{ fontSize: 28, marginBottom: 30 }}>{I18n.t('introduction_title')}</TextField>
                <Card style={{ marginLeft: 30, marginRight: 30 }}>
                    <CardSection>
                        <EditTextField
                            ref="emailEditTextField"
                            label={I18n.t('email')}
                            minTextLength={3}
                            maxTextLength={30}
                            validationRegex={EMAIL_REGEX}
                            errorMessage={I18n.t('email_error')}
                            placeholder={I18n.t('email_placeholder')}
                            onChangeText={this.onEmailChange.bind(this)}
                            value={this.props.email}
                        />
                    </CardSection>

                    <CardSection>
                        <EditTextField
                            secureTextEntry
                            label={I18n.t('password')}
                            placeholder={I18n.t('password_placeholder')}
                            onChangeText={this.onPasswordChange.bind(this)}
                            value={this.props.password}
                        />
                    </CardSection>
                </Card>
                <Text style={styles.errorTextStyle}>
                    {this.props.error}
                </Text>
                {this.renderButton()}
            </View>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'red',
        margin: 15
    }
};

/**
 * Get some state into our component
 */
const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;
    return { email, password, error, loading };
};

export default connect(mapStateToProps, {
    emailChanged, passwordChanged, loginUser
})(LoginScreen);
