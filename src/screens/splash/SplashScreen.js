import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RoundedImage } from '../../components/common';
import KeychainManager from '../../managers/KeychainManager';
import { SECONDARY_COLOR } from '../../AppConstants';

class SplashScreen extends Component {

    checkCredentials() {
        KeychainManager.getCredentials()
            .then(credentials => {
                if (credentials) {
                    Actions.authScene();
                    //Actions.mainScene({ type: 'reset' });
                    return;
                }
                Actions.authScene();
            });
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <RoundedImage
                    imageSource={{ uri: 'https://upload.wikimedia.org/wikipedia/en/thumb/9/92/React-native-icon.svg/1280px-React-native-icon.svg.png' }}
                    imageSize={200}
                />
                {this.checkCredentials()}
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: SECONDARY_COLOR
    }
};

export default SplashScreen;
