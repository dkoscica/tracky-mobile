import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CardSection, RoundedImage } from '../../components/common';

class EventListItem extends Component {

    onRowPress() {
        Actions.eventTabScene({ event: this.props.event, showSecretCode: true });
    }

    render() {
        const { name, locationname, address, startDateTime, endDateTime } = this.props.event;
        return (
            <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                <View style={styles.containerStyle}>
                    <CardSection>
                        <RoundedImage imageSource={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }} imageSize={100} />
                        <View style={styles.textContainerStyle} >
                            <Text style={styles.titleStyle}>
                                {`${name}`}
                            </Text>
                            <Text style={styles.textStyle}>
                                {locationname}
                            </Text>
                            <Text style={styles.textStyle}>
                                {address}
                            </Text>
                            <Text style={styles.textStyle}>
                                {/*{startDateTime}*/}
                                17.3.2017 18h
                            </Text>
                            <Text style={styles.textStyle}>
                                {/*{endDateTime}*/}
                                17.3.2017 20h
                            </Text>
                        </View>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {

    containerStyle: {
        flex: 3,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.6,
        shadowRadius: 5,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    },

    textContainerStyle: {
        flex: 2,
        alignSelf: 'center',
        marginLeft: 15,
    },

    titleStyle: {
        fontSize: 15,
        fontWeight: '500',
        marginTop: 5,
    },

    textStyle: {
        fontSize: 14,
        marginTop: 5,
    }
};

export default EventListItem;
