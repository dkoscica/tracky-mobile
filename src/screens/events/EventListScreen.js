import React, { Component } from 'react';
import { ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { eventsFetch } from '../../actions';
import UserListItem from './EventListItem';
import { PLATFORM_SPECIFIC_CONTENT_INSET, PLATFORM_SPECIFIC_CONTENT_OFFSET } from '../../AppConstants';

class EventListScreen extends Component {

    componentWillMount() {
        this.props.eventsFetch();
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(nextProps) {
        // nextProps are the next set of props that this component
        // will be rendered with
        // this.props is still the old set of props
        this.createDataSource(nextProps);
    }

    createDataSource({ events }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(events);
    }

    renderRow(event) {
        return <UserListItem event={event} />;
    }

    render() {
        return (
            <ListView
                contentInset={PLATFORM_SPECIFIC_CONTENT_INSET}
                contentOffset={PLATFORM_SPECIFIC_CONTENT_OFFSET}
                style={{ margin: 10, backgroundColor: 'transparent' }}
                enableEmptySections
                dataSource={this.dataSource}
                renderRow={this.renderRow}
            />
        );
    }
}

const mapStateToProps = state => {
    const events = _.map(state.events, (val, uid) => {
        return { ...val, uid };
    });
    return { events };
};

export default connect(mapStateToProps, { eventsFetch })(EventListScreen);
