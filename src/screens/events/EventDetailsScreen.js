import _ from 'lodash';
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import I18n from '../../config/i18n';
import { connect } from 'react-redux';
import { eventUpdate, joinEvent } from '../../actions';
import { Card, Button, CardSection, EditTextField, MessageDialog } from '../../components/common';
import { SECONDARY_COLOR, PLATFORM_SPECIFIC_CONTENT_INSET, PLATFORM_SPECIFIC_CONTENT_OFFSET } from '../../AppConstants';

class EventDetailsScreen extends Component {

    state = {
        showModal: false,
        modalText: ''
    }

    componentDidMount() {
        _.each(this.props.event, (value, prop) => {
            this.props.eventUpdate({ prop, value });
        });
    }

    onAccept() {
        this.setState({ showModal: false });
    }

    joinEvent() {
        const success = false;
        //const message = this.props.eventSuccessfullyJoined ? I18n.t('join_event_successful') : I18n.t('join_event_failed')

        const eventId = 1;
        const userId = 1;

        this.props.joinEvent(eventId, userId);

        // this.setState({
        //     showModal: !this.state.showModal,
        //     modalText: message
        // });
    }

    rendersecretCode() {
        return (<CardSection>
            <EditTextField
                editable={false}
                label={I18n.t('secret_code')}
                value={this.props.secretCode}
                onChangeText={value => this.props.eventUpdate({ prop: 'secretCode', value })}
            />
        </CardSection>);
    }

    render() {
        return (
            <ScrollView
                style={{ marginLeft: 10, marginRight: 10, marginTop: 20, marginBottom: 60 }}
                contentInset={PLATFORM_SPECIFIC_CONTENT_INSET}
                contentOffset={PLATFORM_SPECIFIC_CONTENT_OFFSET} >
                <Card>

                    <View>
                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('event_name')}
                                value={this.props.name}
                                onChangeText={value => this.props.eventUpdate({ prop: 'name', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('participants')}
                                value={`0/${String(this.props.maxNumberOfParticipants)}`}
                                onChangeText={value => this.props.eventUpdate({ prop: 'maxNumberOfParticipants', value })}
                            />
                        </CardSection>

                        {this.props.showsecretCode ? this.rendersecretCode() : null}

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('location_name')}
                                value={this.props.locationname}
                                onChangeText={value => this.props.eventUpdate({ prop: 'locationname', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('address')}
                                value={this.props.address}
                                onChangeText={value => this.props.eventUpdate({ prop: 'address', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('latitude')}
                                value={String(this.props.latitude)}
                                onChangeText={value => this.props.eventUpdate({ prop: 'latitude', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('longitude')}
                                value={String(this.props.longitude)}
                                onChangeText={value => this.props.eventUpdate({ prop: 'longitude', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('startDateTime')}
                                value={this.props.startDateTime}
                                onChangeText={value => this.props.eventUpdate({ prop: 'startDateTime', value })}
                            />
                        </CardSection>

                        <CardSection>
                            <EditTextField
                                editable={false}
                                label={I18n.t('endDateTime')}
                                value={this.props.endDateTime}
                                onChangeText={value => this.props.eventUpdate({ prop: 'endDateTime', value })}
                            />
                        </CardSection>
                    </View>

                    <MessageDialog
                        visible={this.state.showModal}
                        onAccept={this.onAccept.bind(this)}
                    >
                        {this.props.eventSuccessfullyJoined ? I18n.t('join_event_successful') : I18n.t('join_event_failed')}
                    </MessageDialog>

                </Card>

                <Button
                    buttonStyle={{ backgroundColor: SECONDARY_COLOR, marginTop: 20, marginBottom: 20 }}
                    onPress={() => this.joinEvent()}
                >
                    {I18n.t('join_event')}
                </Button>

            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    const { id, name, maxNumberOfParticipants, secretCode, locationname, address, latitude, longitude, startDateTime, endDateTime, eventSuccessfullyJoined } = state.eventDetails;
    return { id, name, maxNumberOfParticipants, secretCode, locationname, address, latitude, longitude, startDateTime, endDateTime, eventSuccessfullyJoined };
};

export default connect(mapStateToProps, {
    eventUpdate, joinEvent
})(EventDetailsScreen);
