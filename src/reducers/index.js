import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import UserFormReducer from './UserFormReducer';
import UsersReducer from './UsersReducer';
import UserDetailsReducer from './UserDetailsReducer';
import EventsReducer from './EventsReducer';
import EventDetailsReducer from './EventDetailsReducer';
import UserLocationReducer from './UserLocationReducer';

export default combineReducers({
    auth: AuthReducer,
    userForm: UserFormReducer,
    users: UsersReducer,
    events: EventsReducer,
    eventDetails: EventDetailsReducer,
    userDetails: UserDetailsReducer,
    userLocation: UserLocationReducer
});
