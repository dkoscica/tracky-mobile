import {
    EVENTS_FETCH_SUCCESS,
    EVENT_UPDATE,
    EVENTS_WEBSOCKET_FETCH_SUCCESS
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    console.log(action.type);
    switch (action.type) {
        case EVENTS_FETCH_SUCCESS:
            return action.payload;

        case EVENTS_WEBSOCKET_FETCH_SUCCESS:
            return action.payload;

        default:
            return state;
    }
};
