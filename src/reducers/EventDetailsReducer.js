import {
    EVENT_UPDATE,
    JOIN_EVENT_SUCCESSFUL,
    JOIN_EVENT_FAILED
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    //console.log(action);
    switch (action.type) {
        case EVENT_UPDATE:
            return { ...state, [action.payload.prop]: action.payload.value };

        case JOIN_EVENT_SUCCESSFUL:
            return { ...state, eventSuccessfullyJoined: true, error: '' };

        case JOIN_EVENT_FAILED:
            return { ...state, eventSuccessfullyJoined: false, error: '' };

        default:
            return state;
    }
};
