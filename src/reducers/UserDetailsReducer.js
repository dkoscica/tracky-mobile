import {
    USER_UPDATE
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USER_UPDATE:
            return { ...state, [action.payload.prop]: action.payload.value };
        default:
            return state;
    }
};
