import {
    LOCATION_DETERMINED
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    //console.log(action);
    switch (action.type) {
        case LOCATION_DETERMINED:
            return action.payload;
        default:
            return state;
    }
};
