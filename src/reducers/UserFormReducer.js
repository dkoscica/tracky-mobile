import {
    USER_CREATE,
    USER_UPDATE,
    USER_CLEAR_FORM,
    USER_REGISTRATION_FAILED
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phoneNumber: ''
};

export default (state = INITIAL_STATE, action) => {
    console.log(action);
    switch (action.type) {
        case USER_UPDATE:
            // action.payload === { prop: 'name', value: 'jane' }
            /**
             * [action.payload.prop]: action.payload.value is key interpolation, its not an array! It's a new ES6 feature 
             * If we call our action with a prop of shift it will be determinated at runtime as 
             * shift: action.payload.value
             */
            return { ...state, [action.payload.prop]: action.payload.value };
        case USER_CREATE:
            return INITIAL_STATE;
        case USER_CLEAR_FORM:
            return INITIAL_STATE;
        case USER_REGISTRATION_FAILED:
            return { ...state, error: 'Registration Failed.' };
        default:
            return state;
    }
};
