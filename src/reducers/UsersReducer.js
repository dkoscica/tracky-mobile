import {
    USERS_FETCH_SUCCESS,
    USERS_WEBSOCKET_FETCH_SUCCESS
} from '../actions/types';

/**
 * We must never return undefined! 
 * Therefore we must have an INITIAL_STATE
 */
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USERS_FETCH_SUCCESS:
            return action.payload;

        case USERS_WEBSOCKET_FETCH_SUCCESS:
            return action.payload;

        default:
            return state;
    }
};
