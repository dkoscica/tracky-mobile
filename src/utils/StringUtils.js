export function isStringLengthValid(text, minTextLength, maxTextLength) {
    return text.length >= minTextLength && text.length < maxTextLength;
}

export function isStringRegexValid(text, regex) {
    return regex.test(text);
}
