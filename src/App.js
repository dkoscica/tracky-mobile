import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';
import { AppState } from 'react-native';
import LocationService from './services/LocationService';
import { registerToSignalR } from './services/WebSocketService';

export const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

class App extends Component {

  state = {
    appState: AppState.currentState
  }

  /**
   * App lifecycle methods
   */
  componentWillMount() {
    LocationService.startLocationTracking();
    registerToSignalR();
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }
    this.setState({ appState: nextAppState });
  }

  render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
