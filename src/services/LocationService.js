import { locationUpdate } from '../actions';
import { Actions } from 'react-native-router-flux';
import { store } from '../App';

const configuration = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 };

export default {
    startLocationTracking() {
        determineCurrentUserLocation();
        trackUserMovement();
    }
};

function determineCurrentUserLocation() {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            const initialPosition = JSON.stringify(position);
            store.dispatch(locationUpdate(initialPosition));
            //alert(initialPosition);
        },
        (error) => alert(JSON.stringify(error)),
        configuration
    );
}

function trackUserMovement() {
    navigator.geolocation.watchPosition(
        (position) => {
            store.dispatch(locationUpdate(position));
        },
        (error) => alert(JSON.stringify(error)),
        configuration
    );
}

