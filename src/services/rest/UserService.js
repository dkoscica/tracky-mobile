
import { SERVER_URL } from './ApiConstants';
import { getRequest, postRequest, putRequest, deleteRequest } from './RESTService';

const USERS_ENDPOINT = `${SERVER_URL}users/`;

export default {

    loginUser(credentials) {
        return postRequest(`${USERS_ENDPOINT}/login`, credentials);
    },

    getUsers() {
        return getRequest(USERS_ENDPOINT);
    },

    getUser(id) {
        return getRequest(`${USERS_ENDPOINT}/${id}`);
    },

    createUser(user) {
        return postRequest(USERS_ENDPOINT, user);
    },

    updateUser(user) {
        return putRequest(USERS_ENDPOINT, user);
    },

    deleteUser(id) {
        return deleteRequest(`${USERS_ENDPOINT}/${id}`);
    }

};




