import { SERVER_URL } from './ApiConstants';
import { getRequest, postRequest, putRequest, deleteRequest } from './RESTService';

const EVENTS_ENDPOINT = `${SERVER_URL}events/`;

export default {

    getEvents() {
        return getRequest(EVENTS_ENDPOINT);
    },

    getEvent(id) {
        return getRequest(`${EVENTS_ENDPOINT}/${id}`);
    },

    createEvent(user) {
        return postRequest(EVENTS_ENDPOINT, user);
    },

    updateEvent(user) {
        return putRequest(EVENTS_ENDPOINT, user);
    },

    deleteEvent(id) {
        return deleteRequest(`${EVENTS_ENDPOINT}/${id}`);
    },

    joinEvent(eventId, userId) {
        return postRequest(EVENTS_ENDPOINT, eventId, userId);
    },

};

