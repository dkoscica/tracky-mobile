/**
 * REST service abstraction built on the Fetch API
 */
export function getRequest(url) {
    return executeRequest('GET', url);
}

export function postRequest(url, data) {
    return executeRequest('POST', url, data);
}

export function putRequest(url, data) {
    return executeRequest('PUT', url, data);
}

export function deleteRequest(url) {
    return executeRequest('DELETE', url);
}

function executeRequest(method, url, data) {
    return new Promise((resolve, reject) => {

        const jsonData = JSON.stringify(data);
        console.log(`jsonData:${jsonData}`);

        const headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });

        fetch(url,
            {
                method,
                headers,
                body: jsonData
            })
            .then((response) => {
                try {
                    return resolve(response.json());
                } catch (error) {
                    reject(error);
                    logError(`Parse error: ${error}`);
                }
            })
            .catch((error) => {
                logError(`Fetch error: ${error}`);
                return reject(error);
            });
    });
}

function logResponse(response) {
    //alert(JSON.stringify(response));
    console.debug(`Response: ${response}`);
}

function logError(error) {
    alert(`Parse error: ${error}`);
    console.debug(`Parse error: ${error}`);
}

