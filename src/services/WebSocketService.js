import signalr from 'react-native-signalr';
import { store } from '../App';
import { eventsWebSocketReceived, usersWebSocketReceived } from '../actions';

const testJson =
    [
        {
            "Name": "Event39",
            "MaxNumberOfParticipants": 43,
            "SecretCode": "90164",
            "LocationName": "Location 63",
            "Address": "Address 55",
            "Latitude": 625,
            "Longitude": 261,
            "StartDateTime": "2017-06-04T22:37:16.3213245",
            "EndDateTime": "2017-06-04T22:37:16.3213245",
            "Participants": [],
            "RegisteredParticipantsCount": 0,
            "Id": 1,
            "DateCreated": "2017-06-04T22:37:16.3213245",
            "DateModified": "2017-06-04T22:37:16.3213245",
            "FormatedDateCreated": "4/6/2017 22:37",
            "FormatedDateModified": "4/6/2017 22:37"
        },
        {
            "Name": "EventFromSignalR",
            "MaxNumberOfParticipants": 100,
            "SecretCode": "90164",
            "LocationName": "Location 63",
            "Address": "Address 55",
            "Latitude": 625,
            "Longitude": 261,
            "StartDateTime": "2017-06-04T22:37:16.3213245",
            "EndDateTime": "2017-06-04T22:37:16.3213245",
            "Participants": [],
            "RegisteredParticipantsCount": 0,
            "Id": 1,
            "DateCreated": "2017-06-04T22:37:16.3213245",
            "DateModified": "2017-06-04T22:37:16.3213245",
            "FormatedDateCreated": "4/6/2017 22:37",
            "FormatedDateModified": "4/6/2017 22:37"
        }
    ];


const connection = signalr.hubConnection('http://dkoscica-001-site3.gtempurl.com/signalr');
connection.logging = true;
const proxy = connection.createHubProxy('userEventHub');

export function registerToSignalR() {
    listenToBroadcastMesssages();
    connect();
    handleConnection();
}

function connect() {
    connection.start().done(() => {
        //store.dispatch(usersWebSocketReceived(testJson));
        //store.dispatch(eventsWebSocketReceived(testJson));

        console.log(`Now connected, connection ID=${connection.id}`);
        // proxy.invoke('helloServer', 'Hello Server, how are you?')
        //     .done((directResponse) => {
        //         console.log('direct-response-from-server', directResponse);
        //     }).fail(() => {
        //         console.warn('Something went wrong when calling server, it might not be up and running?')
        //     });
    }).fail(() => {
        console.log('Failed');
    });
}

function listenToBroadcastMesssages() {
    proxy.on('eventsUpdatedReceived', (data) => {
        console.log('message-from-server', data);
        store.dispatch(eventsWebSocketReceived(data));
    });

    proxy.on('usersUpdatedReceived', (data) => {
        console.log('message-from-server', data);
        store.dispatch(usersWebSocketReceived(data));
    });
}

function handleConnection() {
    connection.connectionSlow(() => {
        console.log('We are currently experiencing difficulties with the connection.');
    });

    connection.error((error) => {
        const errorMessage = error.message;
        let detailedError = '';
        if (error.source && error.source._response) {
            detailedError = error.source._response;
        }
        if (detailedError === 'An SSL error has occurred and a secure connection to the server cannot be made.') {
            console.log('When using react-native-signalr on ios with http remember to enable http in App Transport Security https://github.com/olofd/react-native-signalr/issues/14')
        }
        console.debug(`SignalR error: ' ${errorMessage}, ${detailedError}`);
    });
}
