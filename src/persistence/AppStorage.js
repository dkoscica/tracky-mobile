import {
    AsyncStorage
} from 'react-native';

const STORAGE_NAME = '@TrackyStorage:';

/**
 * Example usage
 * 
   const NAME_KEY = 'email_storage_key';

    AppStorage.saveToStorage(NAME_KEY, 'Dominik')
      .then(() => {
        AppStorage.fetchFromStorage(NAME_KEY)
          .then(value => {
            alert(value);
          });
      });
 */

export default {

    async saveToStorage(key, value) {
        try {
            AsyncStorage.setItem(`${STORAGE_NAME}${key}`, value);
            console.debug(`saveToStorage:${STORAGE_NAME}${key}, ${value}`);
        } catch (error) {
            console.error(error);
        }
    },

    async fetchFromStorage(key) {
        try {
            const value = AsyncStorage.getItem(`${STORAGE_NAME}${key}`);
            if (value !== null) {
                console.debug(`fetchFromStorage:${STORAGE_NAME}${key}, ${value}`);
                return value;
            }
        } catch (error) {
            console.error(error);
        }
    },

    clearStorage() {
        AsyncStorage.clear();
    }

};


