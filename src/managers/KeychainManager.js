import * as Keychain from 'react-native-keychain';
import { SERVER_URL } from '../AppConstants';

export default {

    saveCredentials(username, password) {
        Keychain
            .setInternetCredentials(SERVER_URL, username, password)
            .then(() => {
                console.log('Credentials saved successfully!');
            });
    },

    getCredentials() {
        return new Promise((resolve) => {
            Keychain
                .getInternetCredentials(SERVER_URL)
                .then((credentials) => {
                    if (credentials) {
                        return resolve(credentials);
                    }
                    return resolve(null);
                });
        });
    }
};



