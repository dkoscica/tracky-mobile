/**
 * Auth types
 */
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

/**
 * User types
 */
export const USER_CREATE = 'user_create';
export const USER_UPDATE = 'user_update';
export const USER_CLEAR_FORM = 'user_clear_form';
export const USERS_FETCH_SUCCESS = 'users_fetch_success';
export const USERS_WEBSOCKET_FETCH_SUCCESS = 'users_websocket_fetch_success';
export const USER_REGISTRATION_FAILED = 'user_registration_failed';

/**
 * Event types
 */
export const EVENT_CREATE = 'event_create';
export const EVENT_UPDATE = 'event_update';
export const EVENTS_FETCH_SUCCESS = 'events_fetch_success';
export const EVENTS_WEBSOCKET_FETCH_SUCCESS = 'events_websocket_fetch_success';
export const EVENT_SAVE_SUCCESS = 'event_save_success';
export const EVENT_CLEAR_FORM = 'event_clear_form';
export const JOIN_EVENT_SUCCESSFUL = 'join_event_successful';
export const JOIN_EVENT_FAILED = 'join_event_failed';

/**
 * Location types
 */
export const LOCATION_DETERMINED = 'location_determined';
export const LOCATION_RESETED = 'location_reseted';
