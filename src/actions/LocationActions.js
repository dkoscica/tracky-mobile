import {
    LOCATION_DETERMINED,
    LOCATION_RESETED
} from './types';

export const locationUpdate = (locationData) => ({
        type: LOCATION_DETERMINED,
        payload: { locationData }
    });

export const resetLocation = () => ({
        type: LOCATION_RESETED,
        payload: ''
    });

