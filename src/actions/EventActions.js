import { Actions } from 'react-native-router-flux';
import { API_SUCCESS_STATUS } from '../services/rest/ApiConstants';
import EventService from '../services/rest/EventService';

import {
    EVENT_UPDATE,
    EVENTS_FETCH_SUCCESS,
    EVENTS_WEBSOCKET_FETCH_SUCCESS,
    EVENT_CLEAR_FORM,
    JOIN_EVENT_SUCCESSFUL,
    JOIN_EVENT_FAILED
} from './types';

export function eventClearForm() {
    return {
        type: EVENT_CLEAR_FORM,
        payload: ''
    };
}

export const eventUpdate = ({ prop, value }) => {
    console.log(prop, value);
    return {
        type: EVENT_UPDATE,
        payload: { prop, value }
    };
};

export function eventsFetch() {
    return (dispatch) => {
        EventService.getEvents()
            .then((data) => {
                dispatch({ type: EVENTS_FETCH_SUCCESS, payload: data });
            });
    };
}

/**
 * Join event methods
 */
export function joinEvent(eventId, userId) {
    return (dispatch) => {
        EventService.joinEvent({ eventId, userId })
            .then((response) => {
                if (response.status === API_SUCCESS_STATUS) {
                    joinEventSuccess(dispatch, response.data);
                    return;
                }
                joinEventFail(dispatch);
            })
            .catch((error) => {
                console.log(error);
            });
    };
}

function joinEventSuccess(dispatch, user) {
    dispatch({
        type: JOIN_EVENT_SUCCESSFUL, payload: user
    });
}

function joinEventFail(dispatch) {
    dispatch({ type: JOIN_EVENT_FAILED });
}

/**
 * Websocket methods
 */
export function eventsWebSocketReceived(data) {
    return {
        type: EVENTS_WEBSOCKET_FETCH_SUCCESS,
        payload: data
    };
}
