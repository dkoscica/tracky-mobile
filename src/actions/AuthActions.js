import { Actions } from 'react-native-router-flux';
import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER
} from './types';

import { API_SUCCESS_STATUS } from '../services/rest/ApiConstants';
import UserService from '../services/rest/UserService';
import KeychainManager from '../managers/KeychainManager';

/**
 * Action creators 
 */

// Returns an action which always has a type property, which in this case is a String
export function emailChanged(text) {
    return {
        type: EMAIL_CHANGED,
        payload: text
    };
}

export function passwordChanged(text) {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    };
}

/**
 * Redux thunk returns an function instead of an action, 
 * which allows as to manualy dispatch an action when the async call completes
 */
export function loginUser(credentials) {
    return (dispatch) => {
        const { email, password } = credentials;

        dispatch({ type: LOGIN_USER });
        UserService
            .loginUser({ email, password })
            .then((result) => {
                if (result.status === API_SUCCESS_STATUS) {
                    loginUserSuccess(dispatch, result.data);
                    return;
                }
                loginUserFail(dispatch);
            })
            .catch((error) => {
                loginUserFail(dispatch);
            });
    };
}

function loginUserFail(dispatch) {
    dispatch({ type: LOGIN_USER_FAIL });
}

function loginUserSuccess(dispatch, user) {
    KeychainManager.saveCredentials(user.Email, user.Password);
    dispatch({ type: LOGIN_USER_SUCCESS, payload: user });
    Actions.mainScene();
}

