import { Actions } from 'react-native-router-flux';
import {
    USER_CREATE,
    USER_UPDATE,
    USER_REGISTRATION_FAILED,
    USER_CLEAR_FORM,
    USERS_WEBSOCKET_FETCH_SUCCESS,
    USERS_FETCH_SUCCESS
} from './types';
import { API_SUCCESS_STATUS } from '../services/rest/ApiConstants'
import UserService from '../services/rest/UserService';
import KeychainManager from '../managers/KeychainManager';

export const userUpdate = ({ prop, value }) => {
    console.log(prop, value);
    return {
        type: USER_UPDATE,
        payload: { prop, value }
    };
};

export const userClearForm = () => ({
    type: USER_CLEAR_FORM,
    payload: ''
});

/**
 * Action creators 
 */

/**
 * Redux thunk returns an function instead of an action, 
 * which allows as to manualy dispatch an action when the async call completes
 */
export function createUser(firstName, lastName, email, password, phoneNumber) {
    return (dispatch) => {
        UserService.createUser({ firstName, lastName, email, password, phoneNumber })
            .then((response) => {
                if (response.status === API_SUCCESS_STATUS) {
                    createUserSuccess(dispatch, response.data);
                    return;
                }
                createUserFail(dispatch);
            })
            .catch((error) => {
                console.log(error);
            });
    };
}

function createUserFail(dispatch) {
    dispatch({ type: USER_REGISTRATION_FAILED });
}

function createUserSuccess(dispatch, user) {
    KeychainManager.saveCredentials(user.Email, user.Password);
    dispatch({ 
        type: USER_CREATE, payload: user });
    Actions.mainScene();
}

export function updateUser(firstName, lastName, email, password, phoneNumber) {
    return (dispatch) => {
        UserService.updateUser({ firstName, lastName, email, password, phoneNumber })
            .then((user) => {
                dispatch({ type: USER_UPDATE });
                Actions.main();
            })
            .catch((error) => console.log(error));
    };
}

export function usersFetch() {
    return (dispatch) => {
        UserService.getUsers()
            .then((data) => {
                dispatch({ type: USERS_FETCH_SUCCESS, payload: data });
            });
    };
}

export function fetchLoggedInUser() {
    return (dispatch) => {
        KeychainManager.getCredentials()
            .then(credential => {
                const { username, password } = credential;
                UserService
                    .loginUser({ username, password })
                    .then((user) => {
                        dispatch({ type: USER_UPDATE });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            });
    };
}

export function usersWebSocketReceived(data) {
    return {
        type: USERS_WEBSOCKET_FETCH_SUCCESS,
        payload: data
    };
}

